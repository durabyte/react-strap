import React, { Component } from 'react';
import logo from './logo.svg';
import './css/style.css';
import {Col, Form, FormControl, 
  FormGroup, ControlLabel, Checkbox, 
  Button, Navbar, Nav, NavItem, NavDropdown, 
  MenuItem, Alert, Table} from 'react-bootstrap'
import Select from 'react-select';
import 'react-select/dist/react-select.css';
class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      selectedOption: '',
      jsonList: []
    }
  }

  componentDidMount(){
    fetch("https://api.myjson.com/bins/122zbq", {
      method: "GET"
    })
    .then(response => response.json())
    .then(json => {
      console.log(json);
      this.setState({
        jsonList: json
      });
    })
    .catch (error => {
      console.log(error)
    });
  }
  takeUpClick(){
    console.log("submit button is clicked");
  }
  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
		// selectedOption can be null when the `x` (close) button is clicked
		if (selectedOption) {
    	console.log(`Selected: ${selectedOption.label}`);
		}
  }
  render() {
    const { selectedOption } = this.state;
    const selectList = this.state.jsonList.map(item => {
      return { value: item.name, label: item.name}
    })
    return (
      <div>
        <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <img src={logo} className="App-logo" alt="logo" /> <a href="#brand">React Strap</a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem eventKey={1} href="#">
                Link
              </NavItem>
              <NavItem eventKey={2} href="#">
                Link
              </NavItem>
              <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
                <MenuItem eventKey={3.1}>Action</MenuItem>
                <MenuItem eventKey={3.2}>Another action</MenuItem>
                <MenuItem eventKey={3.3}>Something else here</MenuItem>
                <MenuItem divider />
                <MenuItem eventKey={3.3}>Separated link</MenuItem>
              </NavDropdown>
            </Nav>
            <Nav pullRight>
              <NavItem eventKey={1} href="#">
                Link Right
              </NavItem>
              <NavItem eventKey={2} href="#">
                Link Right
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <div className="container">
          <Alert bsStyle="success">Welcome to React Strap, A React based web application, developed by <code>Durabyte Studio Inc.</code></Alert>
          <Form horizontal>
            <FormGroup controlId="formHorizontalEmail">
              <Col componentClass={ControlLabel} sm={2}>
                Email
              </Col>
              <Col sm={10}>
                <FormControl type="email" placeholder="Email" />
              </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalPassword">
              <Col componentClass={ControlLabel} sm={2}>
                Password
              </Col>
              <Col sm={10}>
                <FormControl type="password" placeholder="Password" />
              </Col>
            </FormGroup>

            <FormGroup>
              <Col smOffset={2} sm={10}>
                <Checkbox>Remember me</Checkbox>
              </Col>
            </FormGroup>

            <FormGroup>
              <Col smOffset={2} sm={10}>
                <Button type="button" bsStyle="danger" onClick={this.takeUpClick}>Sign in</Button>
              </Col>
            </FormGroup>
          </Form>
          <div className="col-md-4 col-md-offset-4"> 
            <div className="row">
              <Select
                name="form-field-name"
                value={selectedOption}
                onChange={this.handleChange}
                options={selectList}
              />
            </div>
            <hr/>
            <div>
            <Table striped bordered condensed hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th colSpan="2">Name</th>
                  <th>Price</th>
                  <th>Tags</th>
                </tr>
              </thead>
              <tbody>
              {this.state.jsonList.map(item => {
                console.log(selectedOption.label)
                if(selectedOption === "" || selectedOption.label === item.name)
                  return (
                    <tr>
                      <td>{item.id}</td>
                      <td colSpan="2">{item.name}</td>
                      <td>{item.price}</td>
                      <td>{item.tags}</td>
                    </tr>
                  )
              })}
              </tbody>
            </Table>
            </div>
          </div>
          
          </div>
      </div>
    );
  }
}

export default App;
