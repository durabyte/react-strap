import React, { Component } from 'react';
//import logo from './logo.svg';
//import './App.css';
import './css/style.css';
//import HomePage from './pages/HomePage';
import HeaderSide from './pages/HeaderSide';

class App extends Component {
  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      good : "",
      value: '',
      valueUser: ''
    }
  }
  answerClick(){
    this.setState({
      good: ""
    });
  }
  handleChange(event) {
    this.setState({
      value: event.target.value
    });
  }
  handleChangeUser(event) {
    this.setState({valueUser: "event.target.valueUser"});
  }

  handleSubmit(event) {
    if(this.state.value.indexOf(" ") >1){
      this.setState({
        good:'A name was submitted: ' + this.state.value + ", Good it is a full name"
      });
      event.preventDefault();
    }
    else{
      this.setState({
        good:'A name was submitted: ' + this.state.value + ", unfortunately, it is not a full name"
      });
      event.preventDefault();
    }
  }
  render() {
    return (
      <div className="App">
        <HeaderSide val='React Strap'/>
        <div className="form-holder">
            <form onSubmit={this.handleSubmit}>
              <p>Full Name</p>
              <div className="texter-container ">
                <input className="text-container " type="text" value={this.state.value} onChange={this.handleChange} />
              </div>
              <div>
                <input type="submit" value="Register" className="btnsub" />
                </div>
            </form>
            <p>{this.state.good}</p>
        </div>
      </div>
    );
  }
}

export default App;
