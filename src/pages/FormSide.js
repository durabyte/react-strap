import React, { Component } from 'react';
import logo from '../logo.svg';
import '../css/style.css';

class HomePage extends Component {
  constructor(props){
    super(props);
    this.state = {
      thisname : "Full name"
    }
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.val.indexOf(" ") >=1 && this.props.val === nextProps.val){
      console.log(nextProps);
      this.setState({
        thisname : this.thisname
      })
    }
    else
    {
      this.setState({
        thisname : "Single name"
      })
    }
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="header-holder">
              <div className="left-holder">
                <img src={logo} className="App-logo " alt="logo" />
              </div>
              <div className="right-holder">
                <h1 className="App-title">{ this.props.val }</h1>
              </div>
          </div>
          
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <p>{this.state.thisname}</p>

      </div>
    );
  }
}

export default HomePage;
